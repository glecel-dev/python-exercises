class Person:
    first_name='test'
    last_name='test'

    def __init__(self, name,surname):
        self.first_name = name
        self.last_name = surname

    def get_fullname(self):
        print(self.first_name+self.last_name)


class Student(Person):
    def get_fullname(self):
        print(self.first_name + self.last_name+'-st')