word = input('Enter  word  : ')

def uppercase(string):
    num_upper = 0
    for letter in string[:4]:
        if letter.upper() == letter:
            num_upper += 1
    if num_upper >= 2:
        return string.upper()
    return string

try:
    print(uppercase(word))
except ValueError:
    print("That's not a valid word, Try Again !")