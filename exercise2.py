num = input('Enter  number : ')
def powerOfTwo(n):
    while (n % 2 == 0):
         n /= 2;
    return n == 1;

try:
    numb =int(num)
    if powerOfTwo(numb):
        print("Number is a power of 2");
    else:
        print("Number is not a power of 2");
except ValueError:
    print("That's not a valid number, Try Again !")